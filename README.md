# jfxs / ansible

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/docker-ansible/badges/master/pipeline.svg)](https://gitlab.com/fxs/docker-ansible/pipelines)
[![Image size](https://fxs.gitlab.io/docker-ansible/docker.svg)](https://hub.docker.com/r/jfxs/ansible)

A lightweight automatically updated and tested multiarch amd64 and arm64 Docker image to run [Ansible](https://docs.ansible.com/ansible/latest/index.html) playbooks. [Ansible-lint](https://docs.ansible.com/ansible-lint/) to check playbooks is also included.

Since Ansible version 2.10, the Docker image contains only ansible-base without any collection. It could be necessary to install them. [See usage](#usage)

This image is automatically updated with the [Automated Continuous Delivery of Docker images](https://medium.com/@fx_s/automated-continuous-delivery-of-docker-images-b4bfa0d09f95) pattern.

## Getting Started

### Prerequisities

In order to run this container you'll need Docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Example to run Ansible playbooks in your current directory:

```shell
docker run -it -v ${PWD}:/ansible jfxs/ansible ansible-playbook playbook.yml
```

To install collections:

```shell
docker run -it -v ${PWD}:/ansible jfxs/ansible /bin/sh -c "ansible-galaxy collection install ansible.utils && ansible-playbook playbook.yml"
```

To check playbooks in your current directory with ansible-lint:

```shell
docker run -it -v ${PWD}:/ansible jfxs/ansible ansible-lint playbook.yml
```

## Built with

Docker latest tag contains:

See versions on [Dockerhub](https://hub.docker.com/r/jfxs/ansible)

Versions of installed software are listed in /etc/VERSIONS file of the Docker image. Example to see them for a specific tag:

```shell
docker run -t jfxs/ansible:2.8.1-1 cat /etc/VERSIONS
```

## Versioning

The Docker tag is defined by the Ansible version used and an increment to differentiate build with the same ansible version:

```text
<ansible_version>-<increment>
```

Example: 2.8.1-1

## Vulnerability Scan

The Docker image is scanned every day with the open source vulnerability scanner [Trivy](https://github.com/aquasecurity/trivy).

The latest vulnerability scan report is available on [Gitlab Security Dashboard](https://gitlab.com/fxs/docker-ansible/-/security/dashboard/?state=DETECTED&state=CONFIRMED&reportType=CONTAINER_SCANNING).

## Find Us

* [Dockerhub](https://hub.docker.com/r/jfxs/ansible)
* [Gitlab](https://gitlab.com/fxs/docker-ansible)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-ansible/blob/master/LICENSE) file for details.
