
DOCKER = docker

IMAGE_FROM  = alpine:latest
IMAGE_BUILT = ansible
IMAGE_SSHD = panubo/sshd:1.3.0

ANSIBLE_NETWORK = ansible-test-network
SSH_HOST = ssh-host

## Docker
## ------
docker-build-push: ## Build docker image. Arguments: [arch=arm64] [vcs_ref=c780b3a] [tag=myTag]
docker-build-push: docker-rmi
	$(eval arch := $(shell if [ -z ${arch} ]; then echo "amd64"; else echo "${arch}"; fi))
	$(eval latest_sha := $(shell docker pull ${IMAGE_FROM} >/dev/null 2>&1 && docker inspect --format='{{index .RepoDigests 0}}' ${IMAGE_FROM}))
	$(eval version := $(shell curl -s https://pypi.org/pypi/ansible-core/json | jq -r '.info.version'))
	$(eval tag := $(shell if [ -z ${tag} ]; then echo "${IMAGE_BUILT}:latest"; else echo "${tag}"; fi))
	$(eval build_date := $(shell date -u +'%Y-%m-%dT%H:%M:%SZ'))
	if [ "${arch}" = "arm64" ]; then \
		docker buildx build --progress plain --no-cache --platform linux/arm64 --build-arg IMAGE_FROM_SHA="${latest_sha}" --build-arg ANSIBLE_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} --push . && \
		docker pull ${tag}; \
	else \
		docker build --no-cache --build-arg IMAGE_FROM_SHA="${latest_sha}" --build-arg ANSIBLE_VERSION=${version} --build-arg VCS_REF=${vcs_ref} --build-arg BUILD_DATE=${build_date} -t ${tag} . && \
		docker push ${tag}; \
	fi

docker-rm: ## Remove all unused containers
docker-rm:
	$(DOCKER) container prune -f

docker-rmi: ## Remove all untagged images
docker-rmi: docker-rm
	$(DOCKER) image prune -f

dockerhub-tag: ## Tag image on Dockerhub. Arguments: init-image-tag publish-image publish-tag
dockerhub-tag:
	test -n "${init-image-tag}"  # Failed if init-image-tag not set
	test -n "${publish-image}"  # Failed if publish-image not set
	test -n "${publish-tag}"  # Failed if publish-tag not set
	$(DOCKER) pull ${init-image-tag}
	$(DOCKER) tag ${init-image-tag} ${publish-image}:${publish-tag}
	$(DOCKER) push ${publish-image}:${publish-tag}
	$(DOCKER) tag ${init-image-tag} ${publish-image}:latest
	$(DOCKER) push ${publish-image}:latest

PHONY: docker-build-push docker-rm docker-rmi dockerhub-tag


## Tests
## ------
checks: ## Run linter checks
checks:
	$(DOCKER) run -t --rm -v "${PWD}:/mnt" hadolint/hadolint:latest hadolint /mnt/Dockerfile

local-tests: ## Run tests. Arguments: [tag=ansible:latest]
local-tests:
	@if [ -z ${tag} ]; then \
    	cd tests && \
		echo "------------------ [versions] ------------------" && \
		ansible --version && \
		ansible-lint --version && \
		echo "------------------ [ansible-lint] ------------------" && \
		ansible-galaxy install -r requirements.yml && ansible-lint --offline --project-dir . --exclude .cache playbook.yml && \
		echo "------------------ [ansible local] ------------------" && \
		ansible-galaxy install -r requirements.yml && ansible-playbook --limit local --vault-password-file vault-password-file.dist -i inventory playbook.yml && \
		echo "------------------ [end tests] ------------------"; \
	else \
		echo "------------------ [versions] ------------------" && \
		$(DOCKER) run -t --rm ${tag} ansible --version && \
		$(DOCKER) run -t --rm ${tag} ansible-lint --version && \
		echo "------------------ [ansible-lint] ------------------" && \
		$(DOCKER) run -t --rm -v ${PWD}/tests:/ansible ${tag} /bin/sh -c "ansible-galaxy install -r requirements.yml && ansible-lint --offline --project-dir . --exclude .cache playbook.yml" && \
		echo "------------------ [ansible local] ------------------" && \
		$(DOCKER) run -t --rm -v ${PWD}/tests:/ansible ${tag} /bin/sh -c "ansible-galaxy install -r requirements.yml && ansible-playbook --limit local --vault-password-file vault-password-file.dist -i inventory playbook.yml" && \
		echo "------------------ [end tests] ------------------"; \
	fi

remote-tests: ## Run tests. Arguments: tag=docker-ansible:latest
remote-tests: docker-rm
	test -n "${tag}"  # Failed if Docker tag not set
	$(DOCKER) network create ${ANSIBLE_NETWORK}
	$(DOCKER) run -d --name ${SSH_HOST} --network ${ANSIBLE_NETWORK} -v ${PWD}/tests/keys/key-test.pub:/root/.ssh/authorized_keys -e SSH_ENABLE_ROOT=true ${IMAGE_SSHD}
	$(DOCKER) exec -t ${SSH_HOST} /bin/sh -c "apk --no-cache add python3"
	cp tests/keys/key-test.pem.vault tests/keys/key-test.pem
	$(DOCKER) run -t --rm -v ${PWD}/tests:/ansible ${tag} ansible-vault decrypt --vault-password-file vault-password-file.dist keys/key-test.pem
	$(DOCKER) run -t --network ${ANSIBLE_NETWORK} --rm -v ${PWD}/tests:/ansible ${tag} /bin/sh -c "ansible-galaxy install -r requirements.yml && ansible-playbook --limit remote --vault-password-file vault-password-file.dist -i inventory playbook.yml"
	rm -f tests/keys/key-test.pem
	-$(DOCKER) stop ${SSH_HOST}
	$(DOCKER) network rm ${ANSIBLE_NETWORK}

PHONY: checks local-tests remote-tests

.DEFAULT_GOAL := help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
