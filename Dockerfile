# hadolint ignore=DL3006
ARG IMAGE_FROM_SHA
# hadolint ignore=DL3007
FROM jfxs/ci-toolkit:latest as ci-toolkit

# hadolint ignore=DL3006
FROM ${IMAGE_FROM_SHA}

ARG IMAGE_FROM_SHA
ARG ANSIBLE_VERSION
ARG BUILD_DATE
ARG VCS_REF="DEV"

ENV container docker

LABEL maintainer="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.title="Ansible" \
    org.opencontainers.image.description="A lightweight docker image to run Ansible playbooks" \
    org.opencontainers.image.authors="FX Soubirou <soubirou@yahoo.fr>" \
    org.opencontainers.image.licenses="GPL-3.0-or-later" \
    org.opencontainers.image.version="${ANSIBLE_VERSION}" \
    org.opencontainers.image.url="https://cloud.docker.com/repository/docker/jfxs/ansible" \
    org.opencontainers.image.source="https://gitlab.com/fxs/docker-ansible" \
    org.opencontainers.image.revision=${VCS_REF} \
    org.opencontainers.image.created=${BUILD_DATE}

COPY --from=ci-toolkit /usr/local/bin/get-local-versions.sh /usr/local/bin/get-local-versions.sh

# hadolint ignore=DL3018
RUN apk --no-cache add \
       ca-certificates \
       git \
       make \
       openssh-client \
       openssl \
       python3 \
       py3-cryptography \
       py3-packaging \
       py3-six \
       rsync \
       sshpass \
       vim

COPY ssh_config /etc/ssh/ssh_config
COPY ansible.cfg /etc/ansible/ansible.cfg

# hadolint ignore=DL3018,DL3013,DL4006
RUN apk --no-cache add --virtual \
       .build-deps \
       build-base \
       libffi-dev \
       openssl-dev \
       python3-dev \
       py3-pip \
 && pip3 install --no-cache-dir --upgrade \
       pip \
       cffi \
 && pip3 install --no-cache-dir \
       ansible-core==${ANSIBLE_VERSION} \
       ansible-lint \
       jmespath \
       netaddr \
       yamllint \
 && /usr/local/bin/get-local-versions.sh -f ${IMAGE_FROM_SHA} -a python3 -p ansible-core,ansible-lint,jmespath,netaddr,yamllint \
 && apk --no-cache del .build-deps

# hadolint ignore=DL3059
RUN addgroup nobody root && \
    mkdir -p /ansible /.ansible /.cache && \
    chgrp -R 0 /ansible /.ansible /.cache && \
    chmod -R g=u /etc/passwd /ansible /.ansible /.cache

WORKDIR /ansible

COPY uid-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["uid-entrypoint.sh"]

USER 10010
CMD ["ansible", "--version"]
